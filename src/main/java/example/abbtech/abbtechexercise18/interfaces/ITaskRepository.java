package example.abbtech.abbtechexercise18.interfaces;


import example.abbtech.abbtechexercise18.dto.TaskCreateDTO;
import example.abbtech.abbtechexercise18.dto.TaskResultDTO;
import example.abbtech.abbtechexercise18.dto.TaskUpdateDTO;
import java.util.List;
public interface ITaskRepository {
    List<TaskResultDTO> getAllTasks();

    boolean deleteTaskByID(int id);

    TaskResultDTO createTask(TaskCreateDTO taskCreateDTO);
    TaskResultDTO updateTask(TaskUpdateDTO taskUpdateDTO);
}

