package example.abbtech.abbtechexercise18.repository;
import example.abbtech.abbtechexercise18.dto.TaskCreateDTO;
import example.abbtech.abbtechexercise18.dto.TaskResultDTO;
import example.abbtech.abbtechexercise18.dto.TaskUpdateDTO;
import example.abbtech.abbtechexercise18.enums.TaskStatus;
import example.abbtech.abbtechexercise18.interfaces.ITaskRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import javax.sql.RowSet;
import java.sql.ResultSet;
import java.util.List;

@Repository
public class TaskRepository implements ITaskRepository {
    private final JdbcTemplate jdbcTemplate;

    public TaskRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<TaskResultDTO> getAllTasks() {
        return jdbcTemplate.query("SELECT * FROM public.tasks",
                ((rs, rowNum) -> new TaskResultDTO(rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("desc"),
                        rs.getTimestamp("created_at"),
                        rs.getTimestamp("updated_at"),
                        TaskStatus.fromValue(rs.getString("status")))));
    }

    @Override
    public boolean deleteTaskByID(int id) {
        int rowsAffected = jdbcTemplate.update(
                """
                        DELETE FROM tasks WHERE id = ?
                        """, id);
        return rowsAffected > 0;
    }

    @Override
    public TaskResultDTO createTask(TaskCreateDTO taskCreateDTO) {
        SqlRowSet rs = jdbcTemplate.queryForRowSet(
                """
                        INSERT INTO tasks(title,desc,created_at,updated_at,status)
                        VALUES (?,?,?,?,?)
                        """, taskCreateDTO.title(),taskCreateDTO.desc(),
                taskCreateDTO.created_at(),taskCreateDTO.updated_at(),taskCreateDTO.status().toString());
        return new TaskResultDTO(rs.getInt("id"),taskCreateDTO.title(),taskCreateDTO.desc(),
                taskCreateDTO.created_at(),taskCreateDTO.updated_at(),taskCreateDTO.status());
    }

    @Override
    public TaskResultDTO updateTask(TaskUpdateDTO taskUpdateDTO) {
        int rowsAffected = jdbcTemplate.update(
                """
                        UPDATE tasks
                        SET title = ?, desc = ?, status = ?, updated_at = current_timestamp
                        WHERE id = ?
                        """,
                taskUpdateDTO.title(), taskUpdateDTO.desc(), taskUpdateDTO.status().toString(), taskUpdateDTO.id());

        if (rowsAffected > 0) {
            return jdbcTemplate.queryForObject(
                    "SELECT * FROM tasks WHERE id = ?",
                    (rs, rowNum) -> new TaskResultDTO(
                            rs.getInt("id"),
                            rs.getString("title"),
                            rs.getString("desc"),
                            rs.getTimestamp("created_at"),
                            rs.getTimestamp("updated_at"),
                            TaskStatus.fromValue(rs.getString("status"))
                    ),
                    taskUpdateDTO.id()
            );
        } else {
            throw new RuntimeException("Task not found with id: " + taskUpdateDTO.id());
        }
    }


}

