package example.abbtech.abbtechexercise18.dto;

import example.abbtech.abbtechexercise18.enums.TaskStatus;

import java.sql.Timestamp;

public record TaskResultDTO (int id, String title, String desc, Timestamp created_at, Timestamp updated_at, TaskStatus status) { }
