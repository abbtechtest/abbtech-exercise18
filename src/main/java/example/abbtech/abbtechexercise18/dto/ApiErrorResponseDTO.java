package example.abbtech.abbtechexercise18.dto;

public record ApiErrorResponseDTO(boolean success, String message) { }

