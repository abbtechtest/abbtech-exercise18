package example.abbtech.abbtechexercise18.dto;

import example.abbtech.abbtechexercise18.enums.TaskStatus;

public record TaskUpdateDTO (int id, String title, String desc, TaskStatus status) {
}
