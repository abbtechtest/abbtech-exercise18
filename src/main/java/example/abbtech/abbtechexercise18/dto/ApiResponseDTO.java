package example.abbtech.abbtechexercise18.dto;

public record ApiResponseDTO <T>(boolean success, String message, T data) {
}
