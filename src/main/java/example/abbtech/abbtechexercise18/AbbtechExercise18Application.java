package example.abbtech.abbtechexercise18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbbtechExercise18Application {

    public static void main(String[] args) {
        SpringApplication.run(AbbtechExercise18Application.class, args);
    }

}
